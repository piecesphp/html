<?php

/**
 * HtmlElementArray.php
 */
namespace PiecesPHP\Core\HTML\Collections;

use PiecesPHP\Core\HTML\HtmlElement;

/**
 * HtmlElementArray - Array de HtmlElement
 *
 * @category     HTML/Collections
 * @package     PiecesPHP\Core
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @version     v.1
 * @copyright   Copyright (c) 2018
 */
class HtmlElementArray extends ElementArray
{
    const APPEND_BY_UNIQUE_ID = 0;
    const APPEND_BY_ORDER_INSERT = 1;

    public function __construct($input = [], int $typeAppend = 0)
    {
        parent::__construct($input, self::TYPE_OBJECT, HtmlElement::class);

        if (count($input) > 0) {
            foreach ($input as $value) {
                $this->append($value, $typeAppend);
            }
        }
    }

    public function append($input, int $typeAppend = 0): void
    {
        $this->validateInput($input);

        switch ($typeAppend) {
            case self::APPEND_BY_ORDER_INSERT:
                parent::append($input);
                break;
            case self::APPEND_BY_UNIQUE_ID:default:
                $this->offsetSet($input->getUniqueID(), $input);
                break;
        }
    }
}
