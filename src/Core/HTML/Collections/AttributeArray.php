<?php

/**
 * AttributeArray.php
 */
namespace PiecesPHP\Core\HTML\Collections;

use PiecesPHP\Core\DataStructures\ArrayOf;
use PiecesPHP\Core\HTML\Attribute;

/**
 * AttributeArray - Array de \PiecesPHP\Core\HTML\Attribute
 *
 * @category     HTML/Collections
 * @package     PiecesPHP\Core
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @version     v.1
 * @copyright   Copyright (c) 2018
 */
class AttributeArray extends ArrayOf
{

    public function __construct($input = [])
    {
        parent::__construct([], self::TYPE_OBJECT, Attribute::class);

        if (count($input) > 0) {
            foreach ($input as $value) {
                $this->append($value);
            }
        }
    }

    public function append($input): void
    {
        $this->validateInput($input);

        $this->offsetSet($input->getName(), $input);
    }
}
