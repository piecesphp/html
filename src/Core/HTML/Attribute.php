<?php

/**
 * Attribute.php
 */
namespace PiecesPHP\Core\HTML;

use PiecesPHP\Core\HTML\Exceptions\MalformedAttributeException;

/**
 * Attribute - Atributos de elemento HTML
 *
 * Funciona como módulo independiente
 * @category     HTML
 * @package     PiecesPHP\Core
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @version     v.1
 * @copyright   Copyright (c) 2018
 * @info Funciona como módulo independiente
 */
class Attribute
{
    /**
     * @var string
     */
    protected $name = '';
    /**
     * @var array
     */
    protected $value = [];
    /**
     * @var string
     */
    protected $separator = ' ';

    /**
     * @param string $name
     * @param string[]|string $value Valor del atributo, si es un string se hará un trim de la cadena
     * y se convertirá en un array usando \s como delimitador. Si es un array debe contener solo
     * strings en caso contrario, dicho valor será ignorado.
     * @param string $separator Separador de valores en la salida
     * @return static
     * @throws MalformedAttributeException si el valor del parámetro value es incorrecto
     */
    public function __construct(string $name, $value = null, string $separator = ' ')
    {
        $this->name = $name;
        $this->separator = $separator;
        $this->setValue($value, $this->separator);
    }

    /**
     * @param string[]|string $value Valor del atributo, si es un string se hará un trim de la cadena
     * y se convertirá en un array usando \s como delimitador. Si es un array debe contener solo
     * strings en caso contrario, dicho valor será ignorado.
     * @param string $separator
     * @return static
     * @throws MalformedAttributeException si el valor del parámetro value es incorrecto
     */
    public function setValue($value = null, string $separator = null)
    {
        if (is_string($value) || is_scalar($value)) {

            $value = is_string($value) ? trim($value) : $value;

            $this->value = explode(is_null($separator) ? $this->separator : $separator, $value);
        } else if (is_array($value)) {

            $value = array_filter($value, function ($v) {
                if (is_string($v) || is_scalar($v)) {
                    return true;
                } else {
                    return false;
                }
            });

            $this->value = $value;
        } else if ($value !== null) {
            throw new MalformedAttributeException();
        }
        return $this;
    }

    /**
     * @param string $separator Separador de valores en la salida
     * @return static
     */
    public function setSeparator(string $separator)
    {
        $this->separator = $separator;
        return $this;
    }

    /**
     * @param mixed $value
     * @return static
     */
    public function appendValue($value)
    {
        if (is_scalar($value)) {
            $this->value[] = $value;
        }
        return $this;
    }

    /**
     * @param bool $outputAsString Si es true devuelve una cadena con el valor del atributo, si es false;
     * un array con cada valor.
     * @param string $separator Carácter que separará cada valor del atributo.
     * @return string|array
     */
    public function getValue(bool $outputAsString = true, string $separator = null)
    {
        if ($outputAsString) {
            return implode(is_null($separator) ? ' ' : $separator, $this->value);
        } else {
            return $this->value;
        }
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        /** @var string */
        $valuaAsString = $this->getValue(true, ' ');
        return $this->getName() . "=\"" . $valuaAsString . "\"";
    }
}
