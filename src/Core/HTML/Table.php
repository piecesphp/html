<?php

/**
 * Table.php
 */
namespace PiecesPHP\Core\HTML;

use PiecesPHP\Core\DataStructures\Exceptions\NotAllowedTypeException;
use PiecesPHP\Core\DataStructures\StringArray;
use PiecesPHP\Core\HTML\Collections\AttributeArray;
use PiecesPHP\Core\HTML\Collections\HtmlElementArray;
use PiecesPHP\Core\HTML\HtmlElement;

/**
 * Table - Table html
 *
 * Funciona como módulo independiente
 * @category     HTML
 * @package     PiecesPHP\Core
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @version     v.1
 * @copyright   Copyright (c) 2018
 * @info Funciona como módulo independiente
 */
class Table
{
    /**
     * @var StringArray
     */
    protected $columnNames = null;

    /**
     * @var AttributeArray
     */
    protected $attributes = null;

    /**
     * @var HtmlElement
     */
    protected $table = null;

    /**
     * @var HtmlElementArray
     */
    protected $rows = null;

    /**
     * @var boolean
     */
    protected $withTfoot = true;

    /**
     * @param StringArray $columnNames
     * @param array $data Si se proporciona este parámetro debe ser un array multidimensional, cada elemento debe ser un array,
     * con la cantidad de elementos igual a la cantidad de columnas. Todos los valores deben ser strings, de no serlo, se
     * intentará convertirlos. De no cumplir con los requisitos, lanza una excepción.
     * Ejemplo: Para una tabla con cuatro columnas un valor válido para $data sería: [['1','2','3','4'],...]
     * @param bool $ignoreAmountValues Si es true no valida que la cantidad de datos por fila sea igual a la cantidad de columnas.
     * @param AttributeArray $attributes
     * @return void
     * @throws \Exception|NotAllowedTypeException
     */
    public function __construct(StringArray $columnNames, array $data = [], AttributeArray $attributes = null, bool $ignoreAmountValues = false)
    {
        $this->table = new HtmlElement('table', '', null, $attributes);

        $this->columnNames = $columnNames;

        $this->rows = new HtmlElementArray();

        foreach ($data as $row) {

            $this->addRow($row, $ignoreAmountValues);

        }
    }

    /**
     * @param bool $boolean
     * @return void
     */
    public function setWithTfoot(bool $boolean)
    {
        $this->withTfoot = $boolean;
    }

    /**
     * @param array $rowData Array con la cantidad de elementos igual a la cantidad de columnas de la tabla.
     * Todos los valores deben ser strings, de no serlo, se intentará convertirlos. De no cumplir con
     * los requisitos, lanza una excepción.
     * @param bool $ignoreAmountValues Si es true no valida que la cantidad de datos por fila sea igual a la cantidad de columnas.
     *
     * Ejemplo: Para una tabla con cuatro columnas un valor válido para $data sería: [['1','2','3','4'],...]
     * @return void
     * @throws \Exception|NotAllowedTypeException
     */
    public function addRow(array $rowData, bool $ignoreAmountValues = false)
    {
        if (!is_array($rowData)) {
            throw new \Exception('$rowData debe ser un array.');
        }

        if (!$ignoreAmountValues && (count($rowData) > $this->columnNames->count())) {
            throw new \Exception('La cantidad de columnas en presente en $rowData no es consistente con las esperadas.');
        }

        $rowData = array_map(function ($cell) {
            return (string) $cell;
        }, $rowData);

        $rowData = new StringArray($rowData);

        $tr = new HtmlElement('tr');

        foreach ($rowData as $cell) {
            $tr->appendChild(new HtmlElement('td', $cell));
        }

        $this->rows->append($tr, HtmlElementArray::APPEND_BY_ORDER_INSERT);
    }

    /**
     * @param bool $echo Si es true genera salida de texto
     * @param bool $withTfoot Genera la tabla con tfoot
     *
     * @return string
     */
    public function printTable(bool $echo = true, bool $withTfoot = null)
    {

        $trTitles = new HtmlElement('tr');

        foreach ($this->columnNames as $columnName) {
            $trTitles->appendChild(new HtmlElement('th', $columnName));
        }

        $thead = new HtmlElement('thead', '', $trTitles);
        $tbody = new HtmlElement('tbody', '', $this->rows);
        $tfoot = new HtmlElement('tfoot', '', $trTitles);

        $table = $this->table;

        $table->appendChild($thead);
        $table->appendChild($tbody);

        $withTfoot = !is_null($withTfoot) ? $withTfoot : $this->withTfoot;

        if ($withTfoot) {
            $table->appendChild($tfoot);
        }

        return $table->render($echo);
    }
}
